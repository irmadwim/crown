package com.example.crown.ui.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crown.R;

import java.util.ArrayList;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {

    private ArrayList<String> pertanyaanList; //Digunakan untuk Pertanyaan
    private RadioButton lastCheckedRB = null;

    //Membuat Konstruktor pada Class RecyclerViewAdapter
    public TestAdapter(ArrayList<String> pertanyaanList){

        this.pertanyaanList = pertanyaanList;
    }

    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView NamaAnak, TanggalLahir, Umur;
        private ImageView AvatarAnak;
//        private LinearLayout ItemList;

        ViewHolder(View itemView) {
            super(itemView);
            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            AvatarAnak = itemView.findViewById(R.id.avatar_anak);
            NamaAnak = itemView.findViewById(R.id.textview_nama);
            TanggalLahir = itemView.findViewById(R.id.textview_ttl);
            Umur = itemView.findViewById(R.id.textview_umur);
//            ItemList = itemView.findViewById(R.id.item_list);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_home, parent, false);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(final CoachListViewHolder holder, final int position) {
        holder.priceRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = checked_rb;
            }
        });

    @Override
    public int getItemCount() {
        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return namaList.size();
    }
    public int getItemCount2(){
        return ttlList.size();
    }
    public int getItemCount3(){
        return umurList.size();
    }
    public int getItemCount4(){
        return imageList.size();
    }

}
