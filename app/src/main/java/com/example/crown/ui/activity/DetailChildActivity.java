package com.example.crown.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.crown.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DetailChildActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_child);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        TextView mTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar_detail);

//        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });

        FloatingActionButton fab= (FloatingActionButton) findViewById(R.id.fab_tambahdetail_btn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailChildActivity.this, AddMeasurementActivity.class);
                startActivity(i);
            }
        });
    }
}
