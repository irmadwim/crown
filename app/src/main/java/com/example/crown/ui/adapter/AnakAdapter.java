package com.example.crown.ui.adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crown.R;

import java.util.ArrayList;

public class AnakAdapter extends RecyclerView.Adapter<AnakAdapter.ViewHolder> {

    private ArrayList<String> namaList; //Digunakan untuk Judul
    private ArrayList<String> ttlList; //Digunakan untuk Judul
    private ArrayList<String> umurList; //Digunakan untuk Judul
    private ArrayList<Integer> imageList; //Digunakan untuk Image/Gambar

    //Membuat Konstruktor pada Class RecyclerViewAdapter
    public AnakAdapter(ArrayList<String> namaList, ArrayList<String> ttlList,
                       ArrayList<String> umurList, ArrayList<Integer> imageList){

        this.namaList = namaList;
        this.ttlList = ttlList;
        this.umurList = umurList;
        this.imageList = imageList;
    }

    //ViewHolder Digunakan Untuk Menyimpan Referensi Dari View-View
    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView NamaAnak, TanggalLahir, Umur;
        private ImageView AvatarAnak;
//        private LinearLayout ItemList;

        ViewHolder(View itemView) {
            super(itemView);
            //Menginisialisasi View-View untuk kita gunakan pada RecyclerView
            AvatarAnak = itemView.findViewById(R.id.avatar_anak);
            NamaAnak = itemView.findViewById(R.id.textview_nama);
            TanggalLahir = itemView.findViewById(R.id.textview_ttl);
            Umur = itemView.findViewById(R.id.textview_umur);
//            ItemList = itemView.findViewById(R.id.item_list);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Membuat View untuk Menyiapkan dan Memasang Layout yang Akan digunakan pada RecyclerView
        View V = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_home, parent, false);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        //Memanggil Nilai/Value Pada View-View Yang Telah Dibuat pada Posisi Tertentu
        final String Nama = namaList.get(position);//Mengambil data sesuai dengan posisi yang telah ditentukan
        holder.NamaAnak.setText(Nama);
        final String ttl = ttlList.get(position);
        holder.TanggalLahir.setText(ttl);
        final String umur = umurList.get(position);
        holder.Umur.setText(umur);
        final Integer avatar = imageList.get(position);
        holder.AvatarAnak.setImageResource(avatar);


//        holder.CardPoin.setText("Gambar Meme Ke: "+position);
        //Membuat Aksi Saat Judul Pada List ditekan
//        holder.CardImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Gambar Meme: "+position+" Ditekan", Snackbar.LENGTH_SHORT).show();
//            }
//        });
        //Membuat Aksi Saat List Ditekan
//        holder.ItemList.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Nama Saya: "+Nama, Snackbar.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        //Menghitung Ukuran/Jumlah Data Yang Akan Ditampilkan Pada RecyclerView
        return namaList.size();
    }
    public int getItemCount2(){
        return ttlList.size();
    }
    public int getItemCount3(){
        return umurList.size();
    }
    public int getItemCount4(){
        return imageList.size();
    }

}
