package com.example.crown.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.crown.R;
import com.google.android.material.tabs.TabLayout;


public class NutritionFragment extends Fragment {

    View myFragment;
    ViewPager viewPager;
    TabLayout tabLayout;

    public NutritionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragment = inflater.inflate(R.layout.fragment_nutrition, container, false);

        viewPager = myFragment.findViewById(R.id.listgizi_vp);
        tabLayout = myFragment.findViewById(R.id.listgizi_tablayout);

        return myFragment;
    }


}
