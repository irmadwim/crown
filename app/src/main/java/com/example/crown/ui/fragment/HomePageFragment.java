package com.example.crown.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.crown.ui.activity.AddChildActivity;
import com.example.crown.ui.adapter.AnakAdapter;
import com.example.crown.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;


public class HomePageFragment extends Fragment {

    private ArrayList NamaAnak, TanggalLahir, Umur, Avatar;
    //Daftar Teman
    private String[] namaanak = {"Aditya", "Ryu", "Namira"};
    private String[] tanggallahir = {"1 mei 2002", "3 mei 2007", "12 mei 2016"};
    private String[] umur = {"6 tahun", "6 tahun", "3 tahun"};
    private Integer[] avataranak = {R.drawable.ic_baby_boy, R.drawable.ic_baby_boy, R.drawable.ic_baby_girl};

    public HomePageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);
        NamaAnak = new ArrayList<>();
        TanggalLahir = new ArrayList<>();
        Umur = new ArrayList<>();
        Avatar = new ArrayList<>();
        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
        DaftarItem();

        FloatingActionButton fab = view.findViewById(R.id.fab_tambahanak_btn);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), AddChildActivity.class ));
            }
        });

        //Menggunakan Layout Manager, Dan Membuat List Secara Vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        RecyclerView.Adapter adapter = new AnakAdapter(NamaAnak, TanggalLahir, Umur, Avatar);

        //Membuat Underline pada Setiap Item Didalam List
//        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
//        itemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.line));
//        recyclerView.addItemDecoration(itemDecoration);
        //Memasang Adapter pada RecyclerView
        recyclerView.setAdapter(adapter);

        return view;
    }

    private void DaftarItem(){
        NamaAnak.addAll(Arrays.asList(namaanak));
        TanggalLahir.addAll(Arrays.asList(tanggallahir));
        Umur.addAll(Arrays.asList(umur));
        Avatar.addAll(Arrays.asList(avataranak));
    }

}
