package com.example.crown.ui.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.crown.R;
import com.example.crown.ui.activity.BMIChartActivity;
import com.example.crown.ui.activity.DetailChildActivity;
import com.example.crown.ui.activity.HeadCircumChartActivity;
import com.example.crown.ui.activity.HeightChartActivity;
import com.example.crown.ui.activity.WeightChartActivity;


public class ChildChartFragment extends Fragment {

    CardView cvBerat, cvTinggi, cvLingKepala, cvImt;
    Button btnHistory;
    TextView tvBerat, tvTinggi, tvLingKepala, tvImt;

    public ChildChartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_child_chart, container, false);

        cvBerat = view.findViewById(R.id.card_chart_berat);
        cvTinggi = view.findViewById(R.id.card_chart_tinggi);
        cvLingKepala = view.findViewById(R.id.card_chart_lingkepala);
        cvImt = view.findViewById(R.id.card_chart_imt);

        btnHistory = view.findViewById(R.id.btn_history);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), DetailChildActivity.class ));
            }
        });

        cvBerat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), WeightChartActivity.class ));
            }
        });

        cvTinggi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), HeightChartActivity.class ));
            }
        });

        cvLingKepala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), HeadCircumChartActivity.class ));
            }
        });

        cvImt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getBaseContext(), BMIChartActivity.class ));
            }
        });

        return view;
    }

}
