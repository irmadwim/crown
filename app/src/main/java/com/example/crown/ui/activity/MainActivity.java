package com.example.crown.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.crown.R;
import com.example.crown.ui.fragment.AccountFragment;
import com.example.crown.ui.fragment.ChildChartFragment;
import com.example.crown.ui.fragment.DevelopmentFragment;
import com.example.crown.ui.fragment.HomePageFragment;
import com.example.crown.ui.fragment.NutritionFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new HomePageFragment());

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        // beri listener pada saat item/menu bottomnavigation terpilih
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }
        private boolean loadFragment(Fragment fragment){
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.framelayoutmain, fragment)
                        .commit();
                return true;
            }
            return false;
        }

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment = null;
            switch (menuItem.getItemId()){
                case R.id.navigation_home:
                    fragment = new HomePageFragment();
                    break;
                case R.id.navigation_charts:
                    fragment = new ChildChartFragment();
                    break;
                case R.id.navigation_development:
                    fragment = new DevelopmentFragment();
                    break;
                case R.id.navigation_nutrition:
                    fragment = new NutritionFragment();
                    break;
                case R.id.navigation_account:
                    fragment = new AccountFragment();
                    break;
            }
            return loadFragment(fragment);
        }

}
